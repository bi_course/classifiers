import math


def entropy_calc(typeof):
    """Entropy defined as
    Entropy = -P1 log2(P1) -P2 log2(P2) """

    class1 = int(input('Enter elements of class 1 for ' + typeof + ' : '))
    class2 = int(input('Enter elements of class 2 for ' + typeof + ' : '))

    data = probability([class1, class2])
    p1 = data[0]
    p2 = data[1]

    entropy = -(p1 * math.log2(p1)) - (p2 * math.log2(p2))
    #Probe for print entropies
    print('The entropy for the '+ typeof + ' is: '+str(entropy))

    return [entropy, p1, p2, (class1 + class2)]


def information_gain():
    """Information Gain defined as
    IG(parent,children) = E(parent) - [P(ch1) * E(ch1) + P(ch2) * E(ch2) ....] """

    info_gain = 0
    childrens = int(input('Enter the number of childrens: '))
    parent = entropy_calc('Parent')
    childs = []
    total_child = []

    for child in range(childrens):
        childs.append(entropy_calc(('Child ' + str(child + 1))))

    for child in childs:
        total_child.append([child.__getitem__(0), (child.__getitem__(3) / parent.__getitem__(3))])

    for element in total_child:
        info_gain += element[0] * element[1]

    info_gain = parent[0] - info_gain
    return (info_gain)


def probability(data):
    """Look for the probability of two numbers and return its"""
    p1 = data[0]
    p2 = data[1]

    total_elements = p1 + p2

    return [(p1 / total_elements), (p2 / total_elements)]




if __name__ == '__main__':
    print('The Total IG is: '+str(information_gain()))
